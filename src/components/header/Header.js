import React from "react";
import PropTypes from "prop-types";
import { Container, Row, Col } from "reactstrap";
import _ from "lodash";
import moment from "moment";
import logo from "../../assets/logo.svg";
import "./Header.scss";

const Header = ({ companyInfo }) => (
  <div className="header">
    <Container>
      <Row>
        <Col>
          <a href="/" title={_.get(companyInfo, "companyName")}>
            <img src={logo} alt={_.get(companyInfo, "companyName")} />
          </a>
        </Col>
      </Row>
      <Row>
        <Col xs="8">
          <div className="tagline-text">
            {_.get(companyInfo, "companyMotto")}
          </div>
        </Col>
        <Col xs="4">
          <div className="established-text float-right">
            Since {moment(_.get(companyInfo, "companyEst")).format("YYYY")}
          </div>
        </Col>
      </Row>
    </Container>
  </div>
);

Header.propTypes = {
  companyInfo: PropTypes.object
};

export default Header;
