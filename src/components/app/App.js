import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import _ from "lodash";
import { cmsContentGet } from "../../actions/cmsActions";
import Loader from "../common/loader/Loader";
import Header from "../header/Header";
import RosterGrid from "../roster-grid/RosterGrid";
import EmployeeDetail from "../common/employee-detail/EmployeeDetail";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedEmployeeId: null
    };

    this.onEmployeeSelect = this.onEmployeeSelect.bind(this);
  }

  componentDidMount() {
    this.props.getContent();
  }

  onEmployeeSelect(employeeId) {
    this.setState({
      selectedEmployeeId: employeeId
    });
  }

  getSelectedEmployee(selectedEmployeeId) {
    if (selectedEmployeeId) {
      return _.find(
        this.props.employees,
        employee => employee.id === selectedEmployeeId
      );
    } else {
      return;
    }
  }

  render() {
    const { selectedEmployeeId } = this.state;

    return (
      <div className="roster-app">
        {this.props.loading && <Loader />}
        <Header companyInfo={this.props.companyInfo} />
        <RosterGrid
          employees={this.props.employees}
          onEmployeeSelect={this.onEmployeeSelect}
          selectedEmployeeId={selectedEmployeeId}
        />
        <EmployeeDetail
          employee={this.getSelectedEmployee(selectedEmployeeId)}
          onCancel={this.onEmployeeSelect}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    employees: _.get(state.cms.content, "employees", []),
    companyInfo: _.get(state.cms.content, "companyInfo"),
    loading: state.cms.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getContent: bindActionCreators(cmsContentGet, dispatch)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
