import React from "react";
import "./Loader.scss";

const Loader = () => (
  <div className="loader">
    <div className="loader-inner">
      <label> ●</label>
      <label> ●</label>
      <label> ●</label>
      <label> ●</label>
      <label> ●</label>
    </div>
  </div>
);

export default Loader;
