import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import "./EmployeeCard.scss";

const EmployeeCard = ({
  title,
  description,
  imageUrl,
  onSelect,
  isSelected
}) => {
  return (
    <div
      className={classNames("employee-card", {
        "employee-card-selected": isSelected
      })}
      onClick={onSelect}
    >
      <div className="employee-card-image">
        <img src={imageUrl} alt={title} />
      </div>
      <div className="employee-card-content">
        <div className="employee-card-content-title">{title}</div>
        <div className="employee-card-content-excerpt">{description}</div>
      </div>
    </div>
  );
};

EmployeeCard.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  imageUrl: PropTypes.string,
  onSelect: PropTypes.func
};

export default EmployeeCard;
