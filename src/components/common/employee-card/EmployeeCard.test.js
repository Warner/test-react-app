import React from "react";
import EmployeeCard from "./EmployeeCard";
import renderer from "react-test-renderer";

it("renders correctly", () => {
  const tree = renderer.create(<EmployeeCard />).toJSON();
  expect(tree).toMatchSnapshot();
});
