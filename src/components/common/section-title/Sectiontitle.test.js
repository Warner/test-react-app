import React from "react";
import SectionTitle from "./SectionTitle";
import renderer from "react-test-renderer";

it("renders correctly", () => {
  const tree = renderer.create(<SectionTitle />).toJSON();
  expect(tree).toMatchSnapshot();
});
