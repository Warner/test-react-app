import React from "react";
import PropTypes from "prop-types";
import "./SectionTitle.scss";

const SectionTitle = ({ title }) => (
  <div className="section-title">
    <h1>{title}</h1>
  </div>
);

SectionTitle.propTypes = {
  title: PropTypes.string
};

export default SectionTitle;
