import React from "react";
import PropTypes from "prop-types";
import { Modal, ModalHeader, ModalBody } from "reactstrap";
import _ from "lodash";
import moment from "moment";
import SectionTitle from "../section-title/SectionTitle";
import { fullName } from "../../../util/util";
import "./EmployeeDetail.scss";

const EmployeeDetail = ({ employee, onCancel }) => {
  const employeeName = fullName(
    _.get(employee, "firstName"),
    _.get(employee, "lastName")
  );

  return (
    <Modal
      className="employee-detail"
      backdropClassName="employee-detail-backdrop"
      isOpen={typeof employee !== "undefined"}
      toggle={() => onCancel(null)}
    >
      <ModalHeader toggle={() => onCancel(null)} />

      {employee && (
        <ModalBody>
          <div className="employee-detail-content">
            <div className="employee-detail-content-info">
              <img src={employee.avatar} alt={employeeName} />
              <div className="employee-detail-content-info-stats">
                <div className="job-title">{employee.jobTitle}</div>
                <div>{employee.age}</div>
                <div>{moment(employee.dateJoined).format("MMMM YYYY")}</div>
              </div>
            </div>
            <div className="employee-detail-content-description">
              <SectionTitle title={employeeName} />
              <p>{employee.bio}</p>
            </div>
          </div>
        </ModalBody>
      )}
    </Modal>
  );
};

EmployeeDetail.propTypes = {
  employee: PropTypes.object,
  onCancel: PropTypes.func
};

export default EmployeeDetail;
