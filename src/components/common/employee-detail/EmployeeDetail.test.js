import React from "react";
import EmployeeDetail from "./EmployeeDetail";
import renderer from "react-test-renderer";

it("renders correctly", () => {
  const tree = renderer.create(<EmployeeDetail />).toJSON();
  expect(tree).toMatchSnapshot();
});
