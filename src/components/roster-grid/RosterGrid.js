import React from "react";
import PropTypes from "prop-types";
import { Container, Row, Col } from "reactstrap";
import SectionTitle from "../common/section-title/SectionTitle";
import EmployeeCard from "../common/employee-card/EmployeeCard";
import { excerpt, fullName } from "../../util/util";
import "./RosterGrid.scss";

const RosterGrid = ({ employees, onEmployeeSelect, selectedEmployeeId }) => (
  <div className="roster-grid">
    <Container>
      <SectionTitle title="Our Employees" />
      <div className="roster-grid-content">
        <Row>
          {employees.map(employee => {
            return (
              <Col xs={12} lg={4} key={employee.id}>
                <EmployeeCard
                  title={fullName(employee.firstName, employee.lastName)}
                  description={excerpt(employee.bio)}
                  imageUrl={employee.avatar}
                  onSelect={() => onEmployeeSelect(employee.id)}
                  isSelected={selectedEmployeeId === employee.id}
                />
              </Col>
            );
          })}
        </Row>
      </div>
    </Container>
  </div>
);

RosterGrid.propTypes = {
  employees: PropTypes.array,
  onEmployeeSelect: PropTypes.func,
  selectedEmployeeId: PropTypes.string
};

export default RosterGrid;
