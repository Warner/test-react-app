import React from "react";
import RosterGrid from "./RosterGrid";
import renderer from "react-test-renderer";

const employees = [
  {
    id: 1,
    name: "Bobby"
  },
  {
    id: 2,
    name: "Dave"
  }
];

it("renders correctly", () => {
  const tree = renderer.create(<RosterGrid employees={employees} />).toJSON();
  expect(tree).toMatchSnapshot();
});
