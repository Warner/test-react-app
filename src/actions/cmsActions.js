import * as cmsApi from "../api/cmsApi";
import {
  CMS_CONTENT_LOADING,
  CMS_CONTENT_SUCCESS,
  CMS_CONTENT_FAILED
} from "../constants/actionTypes";

const cmsContentLoading = dispatch => {
  return () => {
    return new Promise(resolve => {
      dispatch({ type: CMS_CONTENT_LOADING });
      // timeout added to show loading
      window.setTimeout(() => {
        resolve();
      }, 3000);
    });
  };
};

const cmsContentSuccess = content => {
  return { type: CMS_CONTENT_SUCCESS, content };
};

const cmsContentFailed = error => {
  return { type: CMS_CONTENT_FAILED, error };
};

export const cmsContentGet = () => {
  return (dispatch, getState) => {
    return dispatch(cmsContentLoading(dispatch))
      .then(() => {
        return cmsApi.cmsContentGet();
      })
      .then(
        response => {
          return dispatch(cmsContentSuccess(response));
        },
        error => {
          return dispatch(cmsContentFailed(error));
        }
      );
  };
};
