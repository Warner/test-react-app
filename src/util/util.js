import _ from "lodash";

export const fullName = (firstName, lastName) => {
  return `${firstName ? firstName : ""} ${lastName ? lastName : ""}`.trim();
};

export const excerpt = text => {
  return _.truncate(text, {
    length: 80,
    separator: /,?\.* +/
  });
};
