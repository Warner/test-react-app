import { CMS_URL } from "../constants/urls";

export const cmsContentGet = () => {
  const request = new Request(CMS_URL, {
    credentials: "same-origin",
    method: "GET"
  });

  return fetch(request).then(response => {
    if (!response.ok) {
      throw Error(response.statusText);
    }
    return response.json();
  });
};
