import reducer from "../cmsReducer";
import initialState from "../initialState";
import {
  CMS_CONTENT_LOADING,
  CMS_CONTENT_SUCCESS,
  CMS_CONTENT_FAILED
} from "../../constants/actionTypes";

const testData = [
  {
    id: 1,
    name: "Test name 1"
  },
  {
    id: 2,
    name: "Test name 2"
  }
];
const dummyState = { ...initialState };
const testError = "Some error";

describe("cms reducer", () => {
  it("should return the initial state", () => {
    expect(reducer(undefined, {})).toEqual(initialState.cms);
  });

  it("should handle CMS_CONTENT_LOADING", () => {
    expect(reducer(dummyState, { type: CMS_CONTENT_LOADING })).toEqual({
      error: null,
      loading: true,
      content: null
    });
  });

  it("should handle CMS_CONTENT_SUCCESS", () => {
    expect(
      reducer(dummyState, { type: CMS_CONTENT_SUCCESS, content: testData })
    ).toEqual({
      error: null,
      loading: false,
      content: testData
    });
  });

  it("should handle CMS_CONTENT_FAILED", () => {
    expect(
      reducer(dummyState, { type: CMS_CONTENT_FAILED, error: testError })
    ).toEqual({
      error: testError,
      loading: false,
      content: null
    });
  });
});
