import initialState from "./initialState";
import {
  CMS_CONTENT_LOADING,
  CMS_CONTENT_SUCCESS,
  CMS_CONTENT_FAILED
} from "../constants/actionTypes";

const cmsReducer = (state = initialState.cms, action) => {
  switch (action.type) {
    case CMS_CONTENT_LOADING:
      return { error: null, loading: true, content: null };
    case CMS_CONTENT_SUCCESS:
      return { error: null, loading: false, content: action.content };
    case CMS_CONTENT_FAILED:
      return { error: action.error, loading: false, content: null };
    default:
      return state;
  }
};

export default cmsReducer;
