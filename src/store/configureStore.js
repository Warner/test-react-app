import { createStore, compose, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import rootReducer from "../reducers/index";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const configureStore = initialState => {
  const middewares = [thunkMiddleware];

  return createStore(
    rootReducer,
    initialState,
    composeEnhancers(applyMiddleware(...middewares))
  );
};
